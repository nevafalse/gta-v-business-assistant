﻿namespace BusinessAssistant.GUI.Overlays
{
    partial class AssistantOverlay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.SmartDraw = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.opacityTrackBar = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.PlayTimeLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.Update = new System.Windows.Forms.Timer(this.components);
            this.BunkerTimerPanel = new System.Windows.Forms.Panel();
            this.BothRadio = new System.Windows.Forms.RadioButton();
            this.ResearchRadio = new System.Windows.Forms.RadioButton();
            this.ManufacturingRadio = new System.Windows.Forms.RadioButton();
            this.bunkerUnitLabel = new System.Windows.Forms.Label();
            this.btnConfirmBunkerStock = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.bunkerStockTrackBar = new System.Windows.Forms.TrackBar();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.DrawTimerOverlay = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.TimerOverlaySmartDraw = new System.Windows.Forms.CheckBox();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.DrawBunkerSetup = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.opacityTrackBar)).BeginInit();
            this.panel4.SuspendLayout();
            this.BunkerTimerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunkerStockTrackBar)).BeginInit();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel14.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.SmartDraw);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.opacityTrackBar);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(29, 37);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(230, 246);
            this.panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Location = new System.Drawing.Point(-1, 95);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(230, 1);
            this.panel3.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Gray;
            this.label3.Location = new System.Drawing.Point(58, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Boolean Options:";
            // 
            // SmartDraw
            // 
            this.SmartDraw.AutoSize = true;
            this.SmartDraw.Checked = true;
            this.SmartDraw.CheckState = System.Windows.Forms.CheckState.Checked;
            this.SmartDraw.ForeColor = System.Drawing.Color.Gray;
            this.SmartDraw.Location = new System.Drawing.Point(9, 131);
            this.SmartDraw.Name = "SmartDraw";
            this.SmartDraw.Size = new System.Drawing.Size(81, 17);
            this.SmartDraw.TabIndex = 4;
            this.SmartDraw.Text = "Smart Draw";
            this.SmartDraw.UseVisualStyleBackColor = true;
            this.SmartDraw.CheckedChanged += new System.EventHandler(this.SmartDraw_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Location = new System.Drawing.Point(-2, 31);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(230, 1);
            this.panel2.TabIndex = 1;
            // 
            // opacityTrackBar
            // 
            this.opacityTrackBar.Location = new System.Drawing.Point(3, 58);
            this.opacityTrackBar.Maximum = 100;
            this.opacityTrackBar.Minimum = 5;
            this.opacityTrackBar.Name = "opacityTrackBar";
            this.opacityTrackBar.Size = new System.Drawing.Size(222, 45);
            this.opacityTrackBar.TabIndex = 2;
            this.opacityTrackBar.TickFrequency = 4;
            this.opacityTrackBar.Value = 95;
            this.opacityTrackBar.Scroll += new System.EventHandler(this.opacityTrackBar_Scroll);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Gray;
            this.label2.Location = new System.Drawing.Point(58, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Overlay Opacity:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Cyan;
            this.label1.Location = new System.Drawing.Point(59, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Overlay Settings";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Black;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.PlayTimeLabel);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Location = new System.Drawing.Point(1120, 37);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(230, 246);
            this.panel4.TabIndex = 6;
            // 
            // PlayTimeLabel
            // 
            this.PlayTimeLabel.AutoSize = true;
            this.PlayTimeLabel.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlayTimeLabel.ForeColor = System.Drawing.Color.Gray;
            this.PlayTimeLabel.Location = new System.Drawing.Point(78, 40);
            this.PlayTimeLabel.Name = "PlayTimeLabel";
            this.PlayTimeLabel.Size = new System.Drawing.Size(40, 17);
            this.PlayTimeLabel.TabIndex = 7;
            this.PlayTimeLabel.Text = "NULL";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Gray;
            this.label4.Location = new System.Drawing.Point(6, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Play Time:";
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Location = new System.Drawing.Point(-2, 31);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(230, 1);
            this.panel6.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Cyan;
            this.label6.Location = new System.Drawing.Point(59, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Useful Information";
            // 
            // Update
            // 
            this.Update.Enabled = true;
            this.Update.Interval = 1000;
            this.Update.Tick += new System.EventHandler(this.Update_Tick);
            // 
            // BunkerTimerPanel
            // 
            this.BunkerTimerPanel.BackColor = System.Drawing.Color.Black;
            this.BunkerTimerPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BunkerTimerPanel.Controls.Add(this.BothRadio);
            this.BunkerTimerPanel.Controls.Add(this.ResearchRadio);
            this.BunkerTimerPanel.Controls.Add(this.ManufacturingRadio);
            this.BunkerTimerPanel.Controls.Add(this.bunkerUnitLabel);
            this.BunkerTimerPanel.Controls.Add(this.btnConfirmBunkerStock);
            this.BunkerTimerPanel.Controls.Add(this.panel8);
            this.BunkerTimerPanel.Controls.Add(this.label5);
            this.BunkerTimerPanel.Controls.Add(this.bunkerStockTrackBar);
            this.BunkerTimerPanel.Controls.Add(this.panel7);
            this.BunkerTimerPanel.Controls.Add(this.label8);
            this.BunkerTimerPanel.Location = new System.Drawing.Point(279, 289);
            this.BunkerTimerPanel.Name = "BunkerTimerPanel";
            this.BunkerTimerPanel.Size = new System.Drawing.Size(230, 186);
            this.BunkerTimerPanel.TabIndex = 8;
            this.BunkerTimerPanel.Visible = false;
            // 
            // BothRadio
            // 
            this.BothRadio.AutoSize = true;
            this.BothRadio.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BothRadio.ForeColor = System.Drawing.Color.Gray;
            this.BothRadio.Location = new System.Drawing.Point(172, 112);
            this.BothRadio.Name = "BothRadio";
            this.BothRadio.Size = new System.Drawing.Size(42, 16);
            this.BothRadio.TabIndex = 11;
            this.BothRadio.TabStop = true;
            this.BothRadio.Text = "Both";
            this.BothRadio.UseVisualStyleBackColor = true;
            this.BothRadio.CheckedChanged += new System.EventHandler(this.BothRadio_CheckedChanged);
            // 
            // ResearchRadio
            // 
            this.ResearchRadio.AutoSize = true;
            this.ResearchRadio.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ResearchRadio.ForeColor = System.Drawing.Color.Gray;
            this.ResearchRadio.Location = new System.Drawing.Point(95, 112);
            this.ResearchRadio.Name = "ResearchRadio";
            this.ResearchRadio.Size = new System.Drawing.Size(63, 16);
            this.ResearchRadio.TabIndex = 10;
            this.ResearchRadio.TabStop = true;
            this.ResearchRadio.Text = "Research";
            this.ResearchRadio.UseVisualStyleBackColor = true;
            this.ResearchRadio.CheckedChanged += new System.EventHandler(this.ResearchRadio_CheckedChanged);
            // 
            // ManufacturingRadio
            // 
            this.ManufacturingRadio.AutoSize = true;
            this.ManufacturingRadio.Checked = true;
            this.ManufacturingRadio.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ManufacturingRadio.ForeColor = System.Drawing.Color.Gray;
            this.ManufacturingRadio.Location = new System.Drawing.Point(3, 112);
            this.ManufacturingRadio.Name = "ManufacturingRadio";
            this.ManufacturingRadio.Size = new System.Drawing.Size(83, 16);
            this.ManufacturingRadio.TabIndex = 9;
            this.ManufacturingRadio.TabStop = true;
            this.ManufacturingRadio.Text = "Manufacturing";
            this.ManufacturingRadio.UseVisualStyleBackColor = true;
            this.ManufacturingRadio.CheckedChanged += new System.EventHandler(this.ManufacturingRadio_CheckedChanged);
            // 
            // bunkerUnitLabel
            // 
            this.bunkerUnitLabel.AutoSize = true;
            this.bunkerUnitLabel.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunkerUnitLabel.ForeColor = System.Drawing.Color.Gray;
            this.bunkerUnitLabel.Location = new System.Drawing.Point(92, 89);
            this.bunkerUnitLabel.Name = "bunkerUnitLabel";
            this.bunkerUnitLabel.Size = new System.Drawing.Size(26, 17);
            this.bunkerUnitLabel.TabIndex = 8;
            this.bunkerUnitLabel.Text = "110";
            // 
            // btnConfirmBunkerStock
            // 
            this.btnConfirmBunkerStock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirmBunkerStock.ForeColor = System.Drawing.Color.Gray;
            this.btnConfirmBunkerStock.Location = new System.Drawing.Point(14, 152);
            this.btnConfirmBunkerStock.Name = "btnConfirmBunkerStock";
            this.btnConfirmBunkerStock.Size = new System.Drawing.Size(200, 23);
            this.btnConfirmBunkerStock.TabIndex = 7;
            this.btnConfirmBunkerStock.Text = "Start Timer";
            this.btnConfirmBunkerStock.UseVisualStyleBackColor = true;
            this.btnConfirmBunkerStock.Click += new System.EventHandler(this.btnConfirmBunkerStock_Click);
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Location = new System.Drawing.Point(1, 143);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(230, 1);
            this.panel8.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Gray;
            this.label5.Location = new System.Drawing.Point(53, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(122, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "Current Stock Level:";
            // 
            // bunkerStockTrackBar
            // 
            this.bunkerStockTrackBar.Location = new System.Drawing.Point(3, 61);
            this.bunkerStockTrackBar.Maximum = 110;
            this.bunkerStockTrackBar.Name = "bunkerStockTrackBar";
            this.bunkerStockTrackBar.Size = new System.Drawing.Size(222, 45);
            this.bunkerStockTrackBar.TabIndex = 2;
            this.bunkerStockTrackBar.Value = 110;
            this.bunkerStockTrackBar.Scroll += new System.EventHandler(this.bunkerStockTrackBar_Scroll);
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Location = new System.Drawing.Point(-2, 31);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(230, 1);
            this.panel7.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Cyan;
            this.label8.Location = new System.Drawing.Point(51, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(118, 17);
            this.label8.TabIndex = 0;
            this.label8.Text = "Bunker Timer Setup";
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.Black;
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.DrawBunkerSetup);
            this.panel11.Controls.Add(this.label12);
            this.panel11.Controls.Add(this.panel14);
            this.panel11.Controls.Add(this.TimerOverlaySmartDraw);
            this.panel11.Controls.Add(this.label11);
            this.panel11.Controls.Add(this.DrawTimerOverlay);
            this.panel11.Controls.Add(this.panel12);
            this.panel11.Controls.Add(this.label13);
            this.panel11.Location = new System.Drawing.Point(29, 289);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(230, 173);
            this.panel11.TabIndex = 9;
            // 
            // panel12
            // 
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.panel13);
            this.panel12.Location = new System.Drawing.Point(-2, 31);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(230, 1);
            this.panel12.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Cyan;
            this.label13.Location = new System.Drawing.Point(42, 7);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(147, 17);
            this.label13.TabIndex = 0;
            this.label13.Text = "Business Timers Settings";
            // 
            // DrawTimerOverlay
            // 
            this.DrawTimerOverlay.AutoSize = true;
            this.DrawTimerOverlay.Checked = true;
            this.DrawTimerOverlay.CheckState = System.Windows.Forms.CheckState.Checked;
            this.DrawTimerOverlay.ForeColor = System.Drawing.Color.Gray;
            this.DrawTimerOverlay.Location = new System.Drawing.Point(14, 70);
            this.DrawTimerOverlay.Name = "DrawTimerOverlay";
            this.DrawTimerOverlay.Size = new System.Drawing.Size(90, 17);
            this.DrawTimerOverlay.TabIndex = 6;
            this.DrawTimerOverlay.Text = "Draw Overlay";
            this.DrawTimerOverlay.UseVisualStyleBackColor = true;
            this.DrawTimerOverlay.CheckedChanged += new System.EventHandler(this.DrawTimerOverlay_CheckedChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Gray;
            this.label11.Location = new System.Drawing.Point(42, 42);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(141, 17);
            this.label11.TabIndex = 12;
            this.label11.Text = "Timer Overlay Settings:";
            // 
            // panel13
            // 
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel13.Location = new System.Drawing.Point(-1, 34);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(230, 1);
            this.panel13.TabIndex = 2;
            // 
            // TimerOverlaySmartDraw
            // 
            this.TimerOverlaySmartDraw.AutoSize = true;
            this.TimerOverlaySmartDraw.ForeColor = System.Drawing.Color.Gray;
            this.TimerOverlaySmartDraw.Location = new System.Drawing.Point(133, 70);
            this.TimerOverlaySmartDraw.Name = "TimerOverlaySmartDraw";
            this.TimerOverlaySmartDraw.Size = new System.Drawing.Size(81, 17);
            this.TimerOverlaySmartDraw.TabIndex = 13;
            this.TimerOverlaySmartDraw.Text = "Smart Draw";
            this.TimerOverlaySmartDraw.UseVisualStyleBackColor = true;
            this.TimerOverlaySmartDraw.CheckedChanged += new System.EventHandler(this.TimerOverlaySmartDraw_CheckedChanged);
            // 
            // panel14
            // 
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel14.Controls.Add(this.panel15);
            this.panel14.Location = new System.Drawing.Point(-1, 98);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(230, 1);
            this.panel14.TabIndex = 3;
            // 
            // panel15
            // 
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel15.Location = new System.Drawing.Point(-1, 34);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(230, 1);
            this.panel15.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Gray;
            this.label12.Location = new System.Drawing.Point(48, 106);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(127, 17);
            this.label12.TabIndex = 14;
            this.label12.Text = "Setup Panel Drawing:";
            // 
            // DrawBunkerSetup
            // 
            this.DrawBunkerSetup.AutoSize = true;
            this.DrawBunkerSetup.ForeColor = System.Drawing.Color.Gray;
            this.DrawBunkerSetup.Location = new System.Drawing.Point(51, 133);
            this.DrawBunkerSetup.Name = "DrawBunkerSetup";
            this.DrawBunkerSetup.Size = new System.Drawing.Size(119, 17);
            this.DrawBunkerSetup.TabIndex = 15;
            this.DrawBunkerSetup.Text = "Draw Bunker Setup";
            this.DrawBunkerSetup.UseVisualStyleBackColor = true;
            this.DrawBunkerSetup.CheckedChanged += new System.EventHandler(this.DrawBunkerSetup_CheckedChanged);
            // 
            // AssistantOverlay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(1372, 573);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.BunkerTimerPanel);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AssistantOverlay";
            this.Opacity = 0.95D;
            this.Text = "AssistantOverlay";
            this.Load += new System.EventHandler(this.AssistantOverlay_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.opacityTrackBar)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.BunkerTimerPanel.ResumeLayout(false);
            this.BunkerTimerPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunkerStockTrackBar)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar opacityTrackBar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox SmartDraw;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Timer Update;
        private System.Windows.Forms.Label PlayTimeLabel;
        private System.Windows.Forms.Panel BunkerTimerPanel;
        private System.Windows.Forms.Button btnConfirmBunkerStock;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TrackBar bunkerStockTrackBar;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label bunkerUnitLabel;
        private System.Windows.Forms.RadioButton ManufacturingRadio;
        private System.Windows.Forms.RadioButton BothRadio;
        private System.Windows.Forms.RadioButton ResearchRadio;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.CheckBox DrawBunkerSetup;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.CheckBox TimerOverlaySmartDraw;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox DrawTimerOverlay;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label13;
    }
}