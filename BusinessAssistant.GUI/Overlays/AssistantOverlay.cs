﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FormOverlayManager;
using BusinessAssistant.Core.Bunker;

namespace BusinessAssistant.GUI.Overlays
{
    public partial class AssistantOverlay : Form
    {
        #region Imports

        [DllImport("user32.dll", SetLastError = true)]
        static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint processId);

        #endregion

        #region Private Fields

        //The process ID of the parent window (window we are overlaying.)
        private uint ParentID;

        //Bunker Timer Vars
        private StaffAssignment bunkerAssignment = StaffAssignment.MANUFACTURING;
        private bool bunkerTimer = false;
        private TimeSpan bunkerRestock = TimeSpan.FromSeconds(0);

        #endregion


        public AssistantOverlay()
        {
            InitializeComponent();
            SetupDrag();  
        }

        #region Setup Methods

        /// <summary>
        /// Setup.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AssistantOverlay_Load(object sender, EventArgs e)
        {
            GetWindowThreadProcessId(OverlayManager.GetOverlay(this.Handle).ParentHandle, out ParentID);
        }

        /// <summary>
        /// Allow all panel controls to be moved by the end-user.
        /// </summary>
        private void SetupDrag()
        {
            foreach (Control c in this.Controls)
            {
                if (c as Panel != null)
                    c.MouseMove += C_MouseMove;
            }
        }

        private void C_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Panel p = sender as Panel;

                p.Location = PointToClient(new Point(Cursor.Position.X, Cursor.Position.Y));
            }
        }

        #endregion

        #region Overlay Settings Panel

        private void opacityTrackBar_Scroll(object sender, EventArgs e)
        {
            this.Opacity = (opacityTrackBar.Value / 100.0f);
        }

        private void SmartDraw_CheckedChanged(object sender, EventArgs e)
        {
            OverlayManager.GetOverlay(this.Handle).SmartDraw = SmartDraw.Checked;
        }

        #endregion

        #region Business Timers Settings Panel

        private void DrawTimerOverlay_CheckedChanged(object sender, EventArgs e)
        {
            frmMain.timerOverlay.Visible = DrawTimerOverlay.Checked;
        }

        private void TimerOverlaySmartDraw_CheckedChanged(object sender, EventArgs e)
        {
            OverlayManager.GetOverlay(frmMain.timerOverlay.Handle).SmartDraw = TimerOverlaySmartDraw.Checked;
        }

        private void DrawBunkerSetup_CheckedChanged(object sender, EventArgs e)
        {
            BunkerTimerPanel.Visible = DrawBunkerSetup.Checked;
        }

        #endregion

        #region Bunker Info Setup Panel

        private void btnConfirmBunkerStock_Click(object sender, EventArgs e)
        {
            bunkerTimer = !bunkerTimer;
            btnConfirmBunkerStock.Text = bunkerTimer ? "Stop Timer" : "Start Timer";

            if (bunkerTimer)
            {
                Int32 sectillrestock = (bunkerStockTrackBar.Value * (Int32)bunkerAssignment);

                bunkerRestock = TimeSpan.FromSeconds(sectillrestock);
            }
            else
            {
                bunkerRestock = TimeSpan.FromSeconds(0);
                frmMain.timerOverlay.UpdateTimerTest(bunkerRestock.ToString());
            }
        }

        private void bunkerStockTrackBar_Scroll(object sender, EventArgs e)
        {
            bunkerUnitLabel.Text = bunkerStockTrackBar.Value.ToString();
        }

        private void ManufacturingRadio_CheckedChanged(object sender, EventArgs e)
        {
            bunkerAssignment = StaffAssignment.MANUFACTURING;
        }

        private void ResearchRadio_CheckedChanged(object sender, EventArgs e)
        {
            bunkerAssignment = StaffAssignment.RESEARCH;
        }

        private void BothRadio_CheckedChanged(object sender, EventArgs e)
        {
            bunkerAssignment = StaffAssignment.BOTH;
        }

        #endregion

        #region Useful Information Panel

        private TimeSpan GetPlayTime()
        {
            if (ParentID == 0)
                return TimeSpan.FromMilliseconds(0);

            return (DateTime.Now - Process.GetProcessById(Convert.ToInt32(ParentID)).StartTime);

        }

        #endregion

     
        private void Update_Tick(object sender, EventArgs e)
        {
            //Useful Info Updates.
            PlayTimeLabel.Text = GetPlayTime().ToString();

            //Bunker Timer Updates.
            if(bunkerTimer)
            {
                bunkerRestock = bunkerRestock.Subtract(TimeSpan.FromSeconds(1));
                frmMain.timerOverlay.UpdateTimerTest(bunkerRestock.ToString());
            }
        }
    }
}
