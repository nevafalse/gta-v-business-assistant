﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessAssistant.GUI.Overlays;
using FormOverlayManager;
using FormOverlayManager.Models;

namespace BusinessAssistant.GUI
{
    public partial class frmMain : Form
    {
        public static TimerOverlay timerOverlay;
        public static AssistantOverlay assistantOverlay;


        public frmMain()
        {
            InitializeComponent();

            timerOverlay = new TimerOverlay();
            assistantOverlay = new AssistantOverlay();


        }

        private void BunkerLabel_Click(object sender, EventArgs e)
        {
            FormOverlay timer = OverlayManager.AddOverlay(timerOverlay, "GTA5", true, Color.LimeGreen);

            FormOverlay assistant = OverlayManager.AddOverlay(assistantOverlay, "GTA5", true, Color.LimeGreen);

            timer.SmartDraw = false;
           // assistant.SmartDraw = false;
        }
    }
}
