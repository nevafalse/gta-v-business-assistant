﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessAssistant.Core.Bunker;

namespace BusinessAssistant.Testing
{
    class Program
    {
        static void Main(string[] args)
        {
            Int32 stockValue = 567000;

            BunkerInfoResult result = Bunker.Calculate(stockValue, false);

            Console.WriteLine($"You have {result.Units} units of stock.");

            Console.WriteLine($"It took you {result.WorkTime} to reach a stock value of ${stockValue}");

            Console.WriteLine($"You have a remaining wait time of {result.RemainingTime} to reach full stock");

            Console.ReadLine();
        }
    }
}
