﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessAssistant.Core.Bunker
{

    /// <summary>
    /// This class contains properties that store bunker logic results.
    /// </summary>
    public class BunkerInfoResult
    {
        /// <summary>
        /// Number of units you have.
        /// </summary>
        public Int32 Units { get; private set; }

        /// <summary>
        /// The time required to reach max stock.
        /// </summary>
        public TimeSpan RemainingTime { get; private set; }

        /// <summary>
        /// The time it took to reach current stock.
        /// </summary>
        public TimeSpan WorkTime { get; private set; }


        public BunkerInfoResult(Int32 cash, bool equalWork)
        {
            Units = BunkerLogic.GetUnits(cash);

            RemainingTime = BunkerLogic.GetRemainingTime(Units, equalWork);

            WorkTime = BunkerLogic.GetWorkTime(Units, equalWork);
        }
    }
}
