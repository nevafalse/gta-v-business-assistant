﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessAssistant.Core.Bunker
{
    public static class Bunker
    {
        /// <summary>
        /// Calculate details for bunker.
        /// </summary>
        /// <param name="cash">The current value of your stock.</param>
        /// <param name="equalWork">If you are manufacturing and researching at the same time.</param>
        /// <returns></returns>
        public static BunkerInfoResult Calculate(Int32 cash, bool equalWork)
        {
            return new BunkerInfoResult(cash, equalWork);
        }
    }
}
