﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessAssistant.Core.Bunker
{
    /// <summary>
    /// This class contains internal bunker data.
    /// </summary>
    internal static class BunkerData
    {
        //Max values.
        internal static readonly Int16 MAX_UNITS = 110;
        internal static readonly Int32 MAX_STOCK_VALUE = 770000;
        internal static readonly Int32 MAX_RESUPPLY_COST = 75000;

        //Tick rates per 1 uint of stock.
        internal static readonly Int16 MANUFACTURING_TICK_RATE = 421;
        internal static readonly Int16 MANUFACTURING_RESEARCH_TICK_RATE = 841;

        //Tick rates per 1 uint of supplys.
        internal static readonly Int16 MANUFACTURING_SUPPLYS_TICK_RATE = 84;
        internal static readonly Int16 RESEARCH_SUPPLYS_TICK_RATE = 210;
        internal static readonly Int16 MANUFACTURING_RESEARCH_SUPPLYS_TICK_RATE = 115;

        internal static readonly Int16 CASH_PER_TICK = 7000;
    }
}
