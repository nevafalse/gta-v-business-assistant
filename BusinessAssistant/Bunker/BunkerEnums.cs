﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessAssistant.Core.Bunker
{
    public enum StaffAssignment
    {
        MANUFACTURING = 84,
        RESEARCH = 210,
        BOTH = 115,
    }
}
