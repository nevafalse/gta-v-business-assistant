﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessAssistant.Core.Bunker
{
    //This class contains internal logic for the Bunker business.
    internal static class BunkerLogic
    {
        /// <summary>
        /// Return the number of units we have.
        /// </summary>
        /// <param name="cash">The current value of your stock.</param>
        /// <returns>Unit count.</returns>
        internal static Int32 GetUnits(Int32 cash)
        {
            return (cash / BunkerData.CASH_PER_TICK);
        }

        /// <summary>
        /// Return the remaining time you have till the bunker has full stock.
        /// </summary>
        /// <param name="units">How many units you have.</param>
        /// <param name="equalWork">If you are manufacturing and researching at the same time.</param>
        /// <returns>Remaining time.</returns>
        internal static TimeSpan GetRemainingTime(Int32 units, bool equalWork)
        {
            return TimeSpan.FromSeconds((BunkerData.MAX_UNITS - units) * 
                (equalWork ? BunkerData.MANUFACTURING_RESEARCH_TICK_RATE : BunkerData.MANUFACTURING_TICK_RATE));

        }

        /// <summary>
        /// Return the time it took to reach the current stock value.
        /// </summary>
        /// <param name="units">How many units you have.</param>
        /// <param name="equalWork">If you are manufacturing and researching at the same time.</param>
        /// <returns>Time taken.</returns>
        internal static TimeSpan GetWorkTime(Int32 units, bool equalWork)
        {
            return TimeSpan.FromSeconds((units * (equalWork ? BunkerData.MANUFACTURING_RESEARCH_TICK_RATE :
                BunkerData.MANUFACTURING_TICK_RATE)));
        }
    }
}
