### What is this repository for? ###

This repository is for GTA V Business Assistant and it's core components.

### What is this program designed for? ###

This program is designed to assist you with your GTA V businesses
by providing details the game doesn't.

### How did you gather the information required to make this program? ###

I gathered tick-rate information from ImOnRedditWow on Reddit.

The rest was gathered by me.
